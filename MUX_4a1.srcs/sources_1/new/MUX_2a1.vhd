----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/14/2020 06:43:38 AM
-- Design Name: 
-- Module Name: MUX_2a1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_2a1 is
    Port ( x0 : in STD_LOGIC;
           x1 : in STD_LOGIC;
           s : in STD_LOGIC;
           f : out STD_LOGIC);
end MUX_2a1;

architecture Behavioral of MUX_2a1 is
begin
f<= x0 when s='0' else x1;
end Behavioral;
