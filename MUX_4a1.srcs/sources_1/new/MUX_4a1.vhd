----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/08/2020 07:01:40 AM
-- Design Name: 
-- Module Name: MUX_4a1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_4a1 is
    Port ( x0 : in STD_LOGIC;
           x1 : in STD_LOGIC;
           x2 : in STD_LOGIC;
           x3 : in STD_LOGIC;
           s : in STD_LOGIC;
           s1 : in STD_LOGIC;
           f : out STD_LOGIC);
end MUX_4a1;

architecture Behavioral of MUX_4a1 is

component MUX_2a1
Port (x0 : in STD_LOGIC;
      x1 : in STD_LOGIC;
      s : in STD_LOGIC;
      f : out STD_LOGIC);
end component;

signal a1,a2: std_logic;
    
begin

M1:mux_2a1 PORT MAP(
    x0=>x0,
    x1=>x1,
    s=>s,
    f=>a1
);
M2:mux_2a1 PORT MAP(
    x0=>x2,
    x1=>x3,
    s=>s,
    f=>a2
);
M3:mux_2a1 PORT MAP(
    x0=>a1,
    x1=>a2,
    s=>s1,
    f=>f
);

end Behavioral;
