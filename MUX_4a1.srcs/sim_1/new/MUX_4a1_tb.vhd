----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/08/2020 07:08:34 AM
-- Design Name: 
-- Module Name: MUX_4a1_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUX_4a1_tb is
   
end MUX_4a1_tb;

architecture Behavioral of MUX_4a1_tb is
component MUX_4a1
Port ( s : in STD_LOGIC;
           s1 : in STD_LOGIC;
           x0 : in STD_LOGIC;
           x1 : in STD_LOGIC;
           x2 : in STD_LOGIC;
           x3 : in STD_LOGIC;
           f : out STD_LOGIC);
end component;
signal s:std_logic :=  '0';
signal s1:std_logic := '0';
signal x0:std_logic := '0';
signal x1:std_logic := '0';
signal x2:std_logic := '0';
signal x3:std_logic := '0';
signal f:std_logic;
begin

    utt:Mux_4a1 PORT MAP(
        s=>s,
        s1=>s1,
        x0=>x0,
        x1=>x1,
        x2=>x2,
        x3=>x3,
        f=>f);

stim_proc: process
begin
    x0<='1';
    x1<='0';
    x2<='1';
    x3<='0';
    wait for 1ms;
    s<='0';
    s1<='0';
    wait for 1ms;
    s<='0';
    s1<='1';
    wait for 1ms;
    s<='1';
    s1<='0';
    wait for 1ms;
    s<='1';
    s1<='1';
    wait;
end process; 
end Behavioral;
